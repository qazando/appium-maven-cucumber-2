package com.test.screen;

import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.remote.RemoteWebElement;

public class ScreenLogin extends BaseScreen {

    @AndroidFindBy(id = "login_username")
    @iOSFindBy(accessibility = "login_username")
    private RemoteWebElement campoemail;

    @AndroidFindBy(id = "login_password")
    @iOSFindBy(accessibility = "login_password")
    private RemoteWebElement camposenha;

    @AndroidFindBy(id = "login_button")
    @iOSFindBy(accessibility = "login_button")
    private RemoteWebElement botaologin;

    public void DadoescreverEmail() {
        campoemail.sendKeys("qazando@gmail.com");
    }

    public void QuandoescreverSenha() {
        camposenha.sendKeys("1234567");
    }

    public void EntaoclicarParaLogar() {
        botaologin.click();
    }

    public void logar() {
        campoemail.sendKeys("qazando@gmail.com");
        camposenha.sendKeys("1234678");
        botaologin.click();
    }

    public void logar2(String email, String Senha) {
        campoemail.sendKeys(email);
        camposenha.sendKeys(Senha);
        botaologin.click();
    }
}