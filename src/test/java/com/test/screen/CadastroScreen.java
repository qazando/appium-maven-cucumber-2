package com.test.screen;

import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.remote.RemoteWebElement;

public class CadastroScreen extends BaseScreen {

    @AndroidFindBy(id = "login_username")
    @iOSFindBy(accessibility = "login_username")
    private RemoteWebElement campoemailcadastro;

    public void preencherEmailCadastro(){
        campoemailcadastro.sendKeys("adsdoifjoaisjdf@gamil.com");
    }
}